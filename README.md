AWS Lambda Child Tagger
=====================
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Create lambda function with CloudFormation.
Python function will be packaged based on listed files and dependencies.
The lambda function will be uploaded to S3 using the S3 bucket provided.

For this lambda function, a log group will also be created so we can control the log expiration date. (otherwise AWS will create it for you with the 'never expires' setting once the lambda is first executed). By default this expiration is set to 7 days, but you can override this.

This function will by default scan known resources (EC2/RDS) for EC2 volumes, ENI's and snapshots and RDS snapshots, picking up the requested `wanted_tags` from the parent resource and copying those tags over to the child. I.e. when an instance is created with the tag "`mcf:environment`" the volumes it creates will not have those tags, but once this lambda runs it will have them.
The purpose of this exercise is cost management, when these tags are present they will make it easier to determine the total cost per environment.
For now this lambda is triggered twice per day by a CloudWatch event rule, later this might be optimized to only trigger on resource creation and be more targetted (and thus more efficient).

This role will create a CloudFormation Stacks with the following resources:
* Cloudwatch Log Group
* Lambda IAM Role
* Lambda IAM Policy
* Lambda Function
* Lambda IAM ManagedPolicy InvokePermission
* Lambda Permission
* Events Rule

![Draw.io](draw.io/services.png)

A test playbook has been supplied which rolls out the lambda function included with this role.
You can run this playbook using the following command:
```bash
ansible-playbook aws-lambda-childtagger/tests/test.yml --inventory aws-lambda-childtagger/tests/inventory.yml
```
This command should run outside of the role dir and requires the aws-utils and aws-lambda role to be in the same root dir as well.

Requirements
------------
Ansible version 2.5.4 or higher  
Python 2.7.x  
Pip 18.x or higher (Python 2.7)

Required python modules:
* boto
* boto3
* awscli
* docker

Dependencies
------------
- aws-utils

Role Variables
--------------
### _Internal_
```yaml
lambda_role_path   : "{{ role_path }}"
lambda_stack_prefix: "lambda"
```
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_abbr    : <aws account generic environment>
environment_type: <environment>
environment_abbr: <environment abbriviation>
```
Role Defaults
--------------
```yaml
---
create_changeset     : True
debug                : False
cloudformation_tags  : {}
tag_prefix           : "mcf"
lambda_log_expiration: 7
lambda_bucket_name   : "{{ account_name }}-{{ aws_region }}-lambda"

aws_lambda_childtagger_params:
  create_changeset  : "{{ create_changeset }}"
  debug             : "{{ debug }}"
  environment_abbr  : "{{ account_abbr }}"
  lambda_bucket_name: "{{ lambda_bucket_name }}"
  log_expiration    : "{{ lambda_log_expiration }}"
  tag_prefix        : "{{ tag_prefix }}:"
  wanted_tags       : "{{ tag_prefix }}:environment {{ tag_prefix }}:environment_abbr {{ tag_prefix }}:role aws:cloudformation:stack-name"
  lambda:
    name: ChildTagger
    files:
      - "{{ lambda_role_path }}/files/lambda.py"
```
Example Playbooks
-----------------
Rollout the aws-lambda-childtagger files with defaults
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"
  roles:
    - aws-lambda
    - aws-lambda-childtagger
```
License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
