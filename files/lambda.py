#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import boto3
import json
import logging
import os
import re
from pprint import pformat
from functools import partial

'''DOCUMENTATION

Concept:
Various AWS resources are created after being provisioned by cloudformation due to the nature of scaling etc.
Not all of these are tagged, notably:
- volumes
- images
- ENI's
- possibly snapshots (EBS and RDS) - these are a bit tricky since they can have multiple users (AMI / volume), but if they have a source we will tag that

This script is triggered by cloudwatch events
- volume creation    - https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-cloud-watch-events.html#volume-events (i.e. when instance starts) "CreateVolume"
- snapshot creation  - https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-cloud-watch-events.html#snapshot-events "CreateSnapshot"
- image creation     - "CreateImage"
- RDS-EVENT-0091/RDS-EVENT-0042 - https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_Events.html

Alternatively a scan mode can be used that grabs all untagged items and gives them a tag (either "unknown" or matching their parent so they'll be skipped next time).
(i.e. commandline mode)

What it does:
- foreach instance
  - check attached volumes, check snapshots for volumes
  - check ENIs
- If a resource is untagged (i.e. no aws:cloudformation:stack-name present)
Determine what belongs where:
How it _should_ be:
- Cloudformation stack tags -> ASG tags -> instance tags -> ENI/EBS Volume tags

So we try to grab all ENI/EBS volumes attached to instances
 - grab their parent's tags as high up the chain as possible and add them (i.e. cloudformation tags if possible, else ASG tags, else instance tags)
Note that these tags -should- propagate, so if they're not on the instance level we do not need to bother, they're missing in that case.

By default we only grab the tags in IncludeTags, which should be:
- mcf:environment
- mcf:environment_abbr
- mcf:role
* aws:cloudformation:stack-name -> nope, would like to, but we are not allowed to create tags that start with aws: :/
Above tags can be adjusted through the IncludeTags environment variable.
If a tag is not present in the parent it is skipped.

If a Name tag is missing it will be filled in based on the parent instanceId and device name.

If a volume has a snapshot make sure to check it, if it's ours we should add our tags to it if not already present.
(overlap - comma seperated ? )

'''


class ChildTagger:
    def __init__(self):
        self.asg    = boto3.client('autoscaling')
        self.ec2    = boto3.client('ec2')
        self.ec2r   = boto3.resource('ec2')
        self.rds    = boto3.client('rds')
        self.events = boto3.client('events')
        self.configure_tags()

    def configure_logger(self, loglevel=logging.INFO, logfile=None):
        logger = logging.getLogger()
        logger.setLevel(loglevel)
        format = "[%(filename)30s:%(funcName)20s():%(lineno)4s:%(levelname)-7s] %(message)s"
        if logfile and logfile is not None:
            logging.basicConfig(level=loglevel,
                                filename=logfile,
                                filemode='w',
                                format=format)
        else:
            logging.basicConfig(level=loglevel, format=format)
        logging.getLogger('boto3').setLevel(logging.WARNING)
        logging.getLogger('botocore').setLevel(logging.WARNING)

    def getEnv(self, name, default):
        if name in os.environ:
            return os.environ[name]
        else:
            return default

    def configure_tags(self):
        self.tags = {}
        tag_prefix = self.getEnv("TagPrefix", "mcf:")
        self.tags['prefix']             = tag_prefix
        self.tags['include_tags'] = self.getEnv("IncludeTags", tag_prefix + "environment" + " " + tag_prefix + "environment_abbr" + " " + tag_prefix + "role" + " " + "aws:cloudformation:stack-name")
        for wanttag in self.tags['include_tags'].split():
            if wanttag.startswith("aws:"):
                logging.debug("Tag {} has a reserved name (starts with aws:) that we can not create! Will switch 'aws:' with prefix '{}' when copying!".format(wanttag, self.tags['prefix']))
        logging.debug("Tags configured as follows: %s" % (pformat(self.tags)))

    def handle(self, event, context):
        logging.debug("Lambda received the event {} with context {}".format(pformat(event), pformat(context)))
        self.event     = event
        self.context = context

        if 'eventName' not in event:
            logging.info("Scan mode since we have no eventName!")
            return self.scanMode()
        logging.info("Event mode since there is an eventName in event: {}!".format(self.event['eventName']))
        try:
            detail        = event['detail']
            eventname = detail['eventName']
            logger.info('eventName    : {}'.format(eventname))
            logger.info('detail         : {}'.format(detail))
        except Exception as e:
            logger.error('Error parsing event {}: {}'.format(pformat(event), pformat(e)))
            return False

    def scanMode(self):
        # Get all instances for source tags etc
        pager = self.ec2.get_paginator('describe_instances')
        response = pager.paginate(
            Filters=[{
                'Name': 'instance-state-name',
                'Values': ['running'],
            }],
        ).build_full_result()

        self.instances = {}
        self.volumes = {}
        for reservation in response['Reservations']:
            if 'Instances' in reservation:
                for instance in reservation['Instances']:
                    self.instances[instance['InstanceId']] = instance
                    itags = {}
                    for tag in instance['Tags']:  # Dammit AWS, fix your API to have a sane dictionary, it's not like duplicate tag keys are allowed or that order matters....
                        itags[tag['Key']] = tag['Value']
                    instance['Tags'] = itags
                    for bd in instance['BlockDeviceMappings']:
                        if 'Ebs' in bd and bd['Ebs']['Status'] == "attached":
                            bd['instanceId'] = instance['InstanceId']
                            self.volumes[bd['Ebs']['VolumeId']] = bd

        # Now we have a list of volumes mapped to instances and their owners. Their tags are in instance['Tags']
        # Tag Volumes
        tagged_volumes = 0
        pager = self.ec2.get_paginator('describe_volumes')
        response = pager.paginate(
            VolumeIds=list(self.volumes.keys())
        ).build_full_result()
        for volume in response['Volumes']:
            volumeId = volume['VolumeId']
            if volumeId not in self.volumes:
                logging.debug("Skipping volume {} in state {}, since it's not in our list of attached volumes! debug:{}".format(volumeId, volume['State'], pformat(volume)))
                continue
            instanceId = self.volumes[volumeId]['instanceId']
            vtags = {}
            if 'Tags' not in volume:
                volume['Tags'] = []
            for tag in volume['Tags']:
                vtags[tag['Key']] = tag['Value']
            self.volumes[volumeId]['tags'] = vtags

            addtags = self.getMissingTags(vtags, self.instances[instanceId]['Tags'])
            # Check if this volume has a name, if not give it one
            if 'Name' not in vtags:
                logging.debug("Name tag not present in volume {} - giving this orphan a name based on instance {}".format(volumeId, instanceId))
                addtags.append({'Key': 'Name', 'Value': instanceId + "-" + self.volumes[volumeId]['DeviceName']})
            if len(addtags) > 0:
                logging.info("Adding to volume {} these tags: {}".format(volumeId, pformat(addtags)))
                bv = self.ec2r.Volume(volumeId)
                try:
                    bv.create_tags(Tags=addtags)
                    tagged_volumes += 1
                except Exception as e:
                    logging.error("Failed to add tags to volume {}: {}".format(volumeId, pformat(e)))
            else:
                logging.debug("No tags to add to volume {}".format(volumeId))
            # TODO Check snapshots
        logging.info("Tagging volumes done")

        # Instance network interfaces
        tagged_enis = 0
        for instanceId in self.instances:
            instance = self.instances[instanceId]
            for eni in instance['NetworkInterfaces']:
                nid = eni['NetworkInterfaceId']
                nic = self.ec2r.NetworkInterface(nid)
                ntags = {}
                for t in nic.tag_set:
                    ntags[t['Key']] = t['Value']
                addtags = self.getMissingTags(ntags, self.instances[instanceId]['Tags'])
                # Check if this volume has a name, if not give it one
                if 'Name' not in ntags:
                    logging.debug("Name tag not present in eni {} - giving this orphan a name based on instance {}".format(nid, instanceId))
                    addtags.append({'Key': 'Name', 'Value': instanceId + "-" + nid})
                if len(addtags) > 0:
                    logging.info("Adding to eni {} these tags: {}".format(nid, pformat(addtags)))
                    try:
                        nic.create_tags(Tags=addtags)
                        tagged_enis += 1
                    except Exception as e:
                        logging.error("Failed to add tags to eni {}: {}".format(nid, pformat(e)))
                else:
                    logging.debug("No tags to add to eni {}".format(nid))
        logging.info("Tagging network interfaces done")

        # RDS Snapshots, Instance-style
        pager = self.rds.get_paginator('describe_db_snapshots')
        response = pager.paginate().build_full_result()
        self.rds_tags = {}  # dbcluster / instance -> tags hash for caching
        tagged_rds_snapshots = 0
        for ss in response['DBSnapshots']:
            sstags = {item['Key']: item['Value'] for item in self.rds.list_tags_for_resource(ResourceName=ss['DBSnapshotArn'])['TagList']}
            dbi = ss['DBInstanceIdentifier']
            dbi_arn = ss['DBSnapshotArn'].replace(ss['DBSnapshotIdentifier'], dbi).replace(':snapshot:', ':db:')
            if ss['DBInstanceIdentifier'] in self.rds_tags:
                # Yay, cache hit
                sourcetags = self.rds_tags[ss['DBInstanceIdentifier']]
            else:
                # No dice, fetch tags.
                try:
                    tmp = self.rds.list_tags_for_resource(ResourceName=dbi_arn)
                except Exception as e:
                    # I refuse to work with 'botocore.errorfactory.DBInstanceNotFoundFault:', error means not found.
                    logging.debug("Could not find source database instance {} for snapshot {} (probably deleted) - skipping".format(ss['DBSnapshotArn'], dbi))
                    # TODO: possibly add empty/'uknown' tags to skip this one in future
                    continue
                sourcetags = {}
                for t in tmp['TagList']:
                    sourcetags[t['Key']] = t['Value']
                self.rds_tags[ss['DBInstanceIdentifier']] = sourcetags

            addtags = self.getMissingTags(sstags, sourcetags)
            if len(addtags) > 0:
                logging.info("Adding to RDS Snapshot {} these tags: {}".format(ss['DBSnapshotArn'], pformat(addtags)))
                try:
                    res = self.rds.add_tags_to_resource(
                        ResourceName=ss['DBSnapshotArn'],
                        Tags=addtags,
                    )
                    tagged_rds_snapshots += 1
                except Exception as e:
                    logging.error("Failed to add tags to rds snapshot {}: {}".format(ss['DBSnapshotArn'], pformat(e)))
            else:
                logging.debug("No tags to add to rds snapshot {}".format(ss['DBSnapshotArn']))
        logging.info("Tagging RDS Snapshots done")

        # RDS Snapshots, Cluster-style
        pager = self.rds.get_paginator('describe_db_cluster_snapshots')
        response = pager.paginate().build_full_result()
        for ss in response['DBClusterSnapshots']:
            sstags = {item['Key']: item['Value'] for item in self.rds.list_tags_for_resource(ResourceName=ss['DBClusterSnapshotArn'])['TagList']}
            dbc = ss['DBClusterIdentifier']
            dbc_arn = ss['DBClusterSnapshotArn'].replace(ss['DBClusterSnapshotIdentifier'], dbc).replace(':cluster-snapshot:', ':cluster:')
            if ss['DBClusterIdentifier'] in self.rds_tags:
                # Yay, cache hit
                sourcetags = self.rds_tags[ss['DBClusterIdentifier']]
            else:
                # No dice, fetch tags.
                try:
                    tmp = self.rds.list_tags_for_resource(ResourceName=dbc_arn)
                except Exception as e:
                    # I refuse to work with 'botocore.errorfactory.DBInstanceNotFoundFault:', error means not found.
                    logging.debug("Could not find source database cluster {} for snapshot {} (probably deleted) - skipping".format(dbc, ss['DBClusterSnapshotArn']))
                    # TODO: possibly add empty/'uknown' tags to skip this one in future
                    continue
                sourcetags = {}
                for t in tmp['TagList']:
                    sourcetags[t['Key']] = t['Value']
                self.rds_tags[ss['DBClusterIdentifier']] = sourcetags

            addtags = self.getMissingTags(sstags, sourcetags)
            if len(addtags) > 0:
                logging.info("Adding to RDS Cluster Snapshot {} these tags: {}".format(ss['DBClusterSnapshotArn'], pformat(addtags)))
                try:
                    res = self.rds.add_tags_to_resource(
                        ResourceName=ss['DBClusterSnapshotArn'],
                        Tags=addtags,
                    )
                    tagged_rds_snapshots += 1
                except Exception as e:
                    logging.error("Failed to add tags to rds cluster snapshot {}: {}".format(ss['DBClusterSnapshotArn'], pformat(e)))
                logging.debug("Add tags result: {}".format(pformat(res)))
            else:
                logging.debug("No tags to add to rds snapshot {}".format(ss['DBClusterSnapshotArn']))
        logging.info("Tagging RDS Cluster Snapshots done")
        logging.info("Tagged a total of {} volumes, {} instances and {} RDS snapshots".format(tagged_volumes, tagged_enis, tagged_rds_snapshots))

    def getMissingTags(self, havetags, sourcetags):
        addtags = []
        for wanttag in self.tags['include_tags'].split():
            if wanttag in havetags:
                continue  # Tag already present
            # We need to figure it out if possible
            if wanttag in sourcetags:
                # Copy it
                addtags.append({'Key': wanttag.replace("aws:", self.tags['prefix']), 'Value': sourcetags[wanttag]})
            else:
                logging.debug("Wanted tag {} not present in sourcetags or havetags - SKIP".format(wanttag))
        return addtags

    def paginated_response(self, func, result_key, next_token=None):
        '''
        Borrowed from Ansible
        Returns expanded response for paginated operations.
        The 'result_key' is used to define the concatenated results that are combined from each paginated response.
        '''
        args = dict()
        if next_token:
            args['NextToken'] = next_token
        response = func(**args)
        result = response.get(result_key)
        next_token = response.get('NextToken')
        if not next_token:
            return result
        return result + self.paginated_response(func, result_key, next_token)
##################################
#    End of ChildTagger Class    #
##################################


def lambda_handler(event, context):
    loglevel         = os.environ['LogLevel'] if 'LogLevel' in os.environ else logging.INFO
    ct = ChildTagger()
    ct.configure_logger(loglevel)
    return ct.handle(event, context)


if __name__ == "__main__":
    ct = ChildTagger()
    ct.configure_logger(logging.DEBUG)
    # import argparse
    # parser = argparse.ArgumentParser(description='AWS Resource Tagger')
    # args = parser.parse_args()
    event = {
        "version": "0",
        "id": "01234567-0123-0123-0123-012345678901",
        "detail-type": "EBS Snapshot Notification",
        "source": "aws.ec2",
        "account": "012345678901",
        "time": "yyyy-mm-ddThh:mm:ssZ",
        "region": "us-east-1",
        "resources": ["arn:aws:ec2::us-west-2:snapshot/snap-01234567"],
        "detail": {
            "event": "createSnapshot",
            "result": "succeeded",
            "cause": "",
            "request-id": "",
            "snapshot_id": "arn:aws:ec2::us-west-2:snapshot/snap-01234567",
            "source": "arn:aws:ec2::us-west-2:volume/vol-01234567",
            "StartTime": "yyyy-mm-ddThh:mm:ssZ",
            "EndTime": "yyyy-mm-ddThh:mm:ssZ"
        }
    }
    print(pformat(ct.handle(event, {})))
